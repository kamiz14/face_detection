import cv2
face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
#read inpu image
img = cv2.imread( r'C:/Users/ccc/Pictures/resized tejlor.jpg')
#conver into grayscale
gray = cv2.cvtColor ( img, cv2.COLOR_BGR2GRAY)
#detect faces
faces = face_cascade.detectMultiScale(gray, 1.1, 10)
#deraw rectange around faces
for (x, y, w, h) in faces:
    cv2.rectangle(img, (x, y), (x+w,  y+h), (255 ,0, 0), 20)
#display the output
    cv2.imshow('img', img)
    cv2.waitKey()
